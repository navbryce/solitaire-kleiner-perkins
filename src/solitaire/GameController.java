package solitaire;

import solitaire.cardutilities.Card;
import solitaire.cardutilities.Hand;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import solitaire.player.HumanPlayer;
import solitaire.player.Player;
import solitaire.solitaireutilities.CompleteStack;
import solitaire.solitaireutilities.Move;
import solitaire.solitaireutilities.SolStack;

public class GameController {
    private Hand dealingDeck;

    // essentially a map where key is suit
    private CompleteStack[] completeStacks;
    private Stack<Card> grabPile;
    private int moveCounter = 0;
    private Player player;
    private SolStack[] stacks;

    public GameController(Player player) {
        dealingDeck = Hand.generateDeck();
        generateStacks();
        generateCompleteStacks();
        grabPile = new Stack();
        this.player = player;

        // start the game
        gameLoop();
    }

    public boolean isGameover() {
        boolean gameOver = true;
        for (CompleteStack stack: completeStacks) {
            gameOver = stack.stackComplete();
            if (!gameOver) {
                break;
            }

        }
        return gameOver;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Moves: " + moveCounter + " \n");
        // convert the suit stacks to string
        toStringSuitStacks(stringBuilder);
        // convert the solitaire stacks to strings
        toStringStacks(stringBuilder);
        // convert grabstack to string
        toStringGrabStack(stringBuilder);

        return stringBuilder.toString();
    }

    public StringBuilder toStringGrabStack(StringBuilder stringBuilder) {
        String cardText = grabPile.size() == 0 ? "" : grabPile.peek().toConsole();
        stringBuilder.append("\n");
        stringBuilder.append("Grab Pile Top: " + cardText);
        stringBuilder.append("\n");
        return stringBuilder;
    }

    public StringBuilder toStringStacks(StringBuilder stringBuilder) {

        String columnSpace = "  ";
        List<Card[]> knownStacks = new LinkedList();
        for (int stackCounter = 0; stackCounter < stacks.length;
             stackCounter++) {
            knownStacks.add(stacks[stackCounter].getKnownStack());
            stringBuilder.append("0" + (stackCounter + 1) + columnSpace);
        }
        stringBuilder.append("\n");

        // any of the stacks could the be longest so...
        int rowCounter = 0;
        // the number of stacks still with cards
        int stacksLeft = stacks.length;
        while (stacksLeft > 0) {
            stacksLeft = 0; // reset the stacks left

            Iterator<Card[]> stackIt = knownStacks.iterator();
            while (stackIt.hasNext()) {
                Card[] cards = stackIt.next();
                if (rowCounter < cards.length) {
                    Card card = cards[rowCounter];
                    String string = card != null ? card.toConsole() + "" : "??";
                    stringBuilder.append(string + columnSpace);
                    stacksLeft++;
                } else {
                    // the list has no more printable cards. Print empty space
                    stringBuilder.append("  " + columnSpace);
                }

            }
            // increment the rower counter and add a new line
            stringBuilder.append("\n");
            rowCounter++;
        }
        return stringBuilder;
    }

    public StringBuilder toStringSuitStacks(StringBuilder builder) {
        int spaceBetweenStacks = 2;
        int stackCharacter = 6;
        builder.append("Suit Stacks: ");
        for (CompleteStack stack: completeStacks) {
            String stackString = String.format("%" + stackCharacter + "s",
                                               stack.toString());
            int spaceCounter = spaceBetweenStacks;
            while (spaceCounter > 0) {
                stackString += " ";
                spaceCounter--;
            }
            builder.append(stackString);
        }
        builder.append("\n\n");

        return builder;
    }

    private boolean chooseMove(Move move, Card[][] completeStacks,
                               Card[][] knownStacks, Card[] grabStack,
                               boolean reshuffle) {
        boolean successful;
        switch (move) {
        case ADD_TO_COMPLETE:
            successful = moveAddToComplete(player.moveToComplete(completeStacks,
                                                    knownStacks));
            break;
        case MOVE_CARD_STACK:
            successful =  moveBetween(player.moveCards(knownStacks));

            break;
        case DRAW_CARD:
            if (!reshuffle) {
                drawCard();
            } else {
                reshuffleGrabPile();
            }
            successful = true;
            break;
        default:
            successful = grabPile.size() > 0 &&
                useGrabCard(player.useGrabStack(grabPile.peek(), knownStacks));
            break;
        }
        return successful;
    }

    private Card[][] cloneCompleteStacks() {
        Card[][] newComplete = new Card[completeStacks.length][];
        for (int stackCounter = 0; stackCounter < completeStacks.length;
             stackCounter++) {
            newComplete[stackCounter] = completeStacks[stackCounter].toArray();
        }
        return newComplete;
    }

    private Card drawCard() {
        return grabPile.push(dealingDeck.removeRandomCard());
    }

    private void gameLoop() {
        boolean previousMoveInvalid = false;
        while (!isGameover()) {
            if (previousMoveInvalid) {
                System.out.println("The previous move was invalid.");
                previousMoveInvalid = false;
            }

            // print the current game
            System.out.println(this);

            // clone structures
            Card[][] completeStacks = cloneCompleteStacks();
            Card[][] knownStacks = getKnownStacks();
            Card[] grabPile = getGrabPile();
            boolean reshuffle = dealingDeck.getNumberOfCards() == 0;

            Move move = player.chooseAction(completeStacks, knownStacks,
                                            grabPile, reshuffle);
            if (chooseMove(move, completeStacks, knownStacks, grabPile,
                           reshuffle)) {
                // If the move was successful, increment the counter
                moveCounter++;
            } else {
                previousMoveInvalid = true;
            }

            System.out.println("\n\n\n\n\n\n\n");

        }

        // game over
        player.addGame(moveCounter);
        System.out.println("Congratulations! You won in " + moveCounter + " moves!");
    }

    private void generateCompleteStacks() {
        // generate a complete stack for each suit
        completeStacks = new CompleteStack[4];
        for (int stackCounter = 0; stackCounter < completeStacks.length;
             stackCounter++) {
            completeStacks[stackCounter] = new CompleteStack(stackCounter);
        }
    }

    private void generateStacks() {
        int maxStack = 7;
        stacks = new SolStack[maxStack];
        for (int stackSize = maxStack; stackSize > 0; stackSize--) {
            List<Card> stackCards = new LinkedList();
            for (int cardCounter = 0; cardCounter < stackSize; cardCounter++) {
                stackCards.add(dealingDeck.removeRandomCard());
            }
            SolStack stack = new SolStack(stackCards);
            stacks[stackSize - 1] = stack;
        }
    }

    private Card[] getGrabPile() {
        return grabPile.toArray(new Card[grabPile.size()]);
    }

    /**
     * Returns a clone of the stack (only of the known stack)
     *
     * @return Returns a non-uniform 2D array
     */
    private Card[][] getKnownStacks() {
        Card[][] knownStacks = new Card[stacks.length][];
        for (int knownCounter = 0; knownCounter < knownStacks.length;
             knownCounter++) {
            knownStacks[knownCounter] = stacks[knownCounter].getKnownStack();
        }
        return knownStacks;
    }

    private boolean moveAddToComplete(int stackNumber) {
        boolean successful;
        if (stackNumber < 1 || stackNumber > 7) {
            successful = false;
        } else {
            SolStack stack = stacks[stackNumber - 1];
            Card[] visibleCards = stack.getVisibleCards();
            if (visibleCards.length > 0) {
                Card card = visibleCards[0];
                CompleteStack completeStack = completeStacks[card.suit];
                if (completeStack.checkMove(card)) {
                    stack.removeVisibleCards(1); // remove the top card
                    completeStack.addCard(card);
                    successful = true;
                } else {
                    successful = false;
                }
            } else {
                successful = false;
            }
        }
        return successful;
    }

    private boolean moveBetween(int[] moveInfo) {
        /*
          First index of moveInfo is originating stack
          Second index of moveInfo is the number of cards to move
          Third index of moveInfo is the the target stack
         */
        boolean success;

        int origin = moveInfo[0] - 1;
        if (origin < 0 || origin > 6) {
            success = false;
        } else {
            SolStack originatingStack = stacks[origin];
            int numberOfCards = moveInfo[1];
            Card[] visibleCards = originatingStack.getVisibleCards();
            if (visibleCards.length < numberOfCards) {
                success = false;
            } else {
                // the cards being moved
                Card[] cardsBeingMoved = new Card[numberOfCards];
                for (int counter = numberOfCards; counter > 0; counter--) {
                    int index = numberOfCards - counter;
                    cardsBeingMoved[index] = visibleCards[index];
                }
                int targetIndex = moveInfo[2] - 1;
                if (targetIndex < 0 || targetIndex > 7) {
                    success = false;
                } else {
                    SolStack targetStack = stacks[targetIndex];
                    if (targetStack.checkMove(cardsBeingMoved)) {
                        originatingStack.removeVisibleCards(numberOfCards);
                        targetStack.addCardsToStack(cardsBeingMoved);
                        success = true;
                    } else {
                        success = false;
                    }
                }
            }
        }
        return success;
    }

    private boolean useGrabCard(int targetId) {
        boolean success;
        if (grabPile.size() == 0) {
            success = false;
        } else {
            Card card = grabPile.peek();
            if (targetId > 0 && targetId <= 7) {
                SolStack targetStack = stacks[targetId - 1];
                Card[] cards = new Card[] {card};
                if (targetStack.checkMove(cards)) {
                    targetStack.addCardsToStack(cards);
                    grabPile.pop();
                    success = true;
                } else {
                    success = false;
                }
            } else if (targetId == -1) {
                // trying to move grab card to complete stack
                CompleteStack completeStack = completeStacks[card.suit];
                if (completeStack.checkMove(card)) {
                    completeStack.addCard(card);
                    grabPile.pop();
                    success = true;
                } else {
                    success = false;
                }
            } else {
                success = false;
            }
        }
        return success;

    }

    private void reshuffleGrabPile() {
        while (!grabPile.empty()) {
            dealingDeck.addCard(grabPile.pop());
        }
    }

    public static void main (String[] args) {
        new GameController(new HumanPlayer());
    }
}
