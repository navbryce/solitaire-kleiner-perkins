package solitaire.cardutilities;

public class Card {
    public static final int CLUBS = 0;
    public static final int HEARTS = 1;
    public static final int SPADES = 2;
    public static final int DIAMONDS = 3;

    public static final int ACE = 1;
    public static final int JACK = 11;
    public static final int QUEEN = 12;
    public static final int KING = 13;

    public final int value;
    public final int suit;


    /**
     * Card constructor
     *
     * @param value [1, 13]
     * @param suit [0, 3]
     */
    public Card(int value, int suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return Card.getValue(value) + " of " + Card.getSuit(suit);
    }

    public String toConsole() {
        return Card.getSuit(suit) + Card.getValue(value);
    }

    public static String getColor(int cardSuit) {
        return cardSuit % 2 == 0 ? "B" : "R";
    }


    /**
     * Returns the card name (value)  as a string
     *
     * @param cardValue the card valuen
     * @return the card value as string
     */
    public static String getValue(int cardValue) {
        String result;
        if (cardValue < 2 || cardValue > 9) {
            switch (cardValue) {
            case 1:
                result = "A";
                break;
            case 10:
                result = "T";
                break;
            case 11:
                result = "J";
                break;
            case 12:
                result = "Q";
                break;
            default:
                result = "K";
                break;
            }
        } else {
            result = "" + cardValue;
        }
        return result;
    }


    /**
     * Returns the suit name based on its identifier
     *
     * @param suitNumber the suit identifier
     * @return the suit name as a string
     */
    public static String getSuit(int suitNumber) {
        String suit;
        switch (suitNumber) {
        case CLUBS:
            suit = "C";
            break;
        case DIAMONDS:
            suit = "D";
            break;
        case HEARTS:
            suit = "H";
            break;
        default:
            suit = "S";
            break;
        }
        return suit;
    }

}
