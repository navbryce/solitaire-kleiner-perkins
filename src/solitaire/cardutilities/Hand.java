package solitaire.cardutilities;

import java.util.LinkedList;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;

public class Hand {

    private List<Card>[] cardMap; // switch to tree if time
    private int numberOfCards = 0;

    public Hand () {
        this(new LinkedList());
    }


    public Hand (List<Card> initialCards) {

        // initialize the card map
        cardMap = new List[13];
        for (int listCounter = 0; listCounter < cardMap.length; listCounter++) {
            cardMap[listCounter] = new LinkedList();
        }

        // store cards in card map
        for (Card card: initialCards) {
            addCard(card);
        }
    }

    public void addCard(Card card) {
        cardMap[card.value - 1].add(card); // zero the card and add to map
        numberOfCards++;
    }

    public void addCards(Card[] cards) {
        for (Card card: cards) {
            addCard(card);
        }
    }

    public List<Card>[] getCardMap () {
        List<Card>[] clone = new List[13];
        for (int cardListCounter = 0; cardListCounter < clone.length;
             cardListCounter++) {
            List<Card> originalList = cardMap[cardListCounter];
            List<Card> cloneList = new LinkedList();
            for (Card card: originalList) {
                // clone the card instance
                cloneList.add(new Card(card.value, card.suit));
            }
            clone[cardListCounter] = cloneList;
        }
        return clone;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }

    public Card[] getSortedArray () {
        Card[] cards = new Card[numberOfCards];
        int counter = 0;
        for (List<Card> cardList: cardMap) {
            for (Card card: cardList) {
                cards[counter] = card;
                counter++;
            }
        }
        return cards;
    }

    public List<Card> getValueList(int cardValue) {
        return cardMap[cardValue - 1];
    }

    /**
     * Removes the card of the same value and suit
     *
     * @param card is the card being removed
     * @return the removed card or null
     */
    public Card removeCard(Card card) {
        List<Card> cardList = getValueList(card.value);
        Card target = null;
        for (int cardCounter = 0; cardCounter < cardList.size()
                 && target == null; cardCounter++) {
            Card possible = cardList.get(cardCounter);
            if (possible.suit == card.suit) {
                target = possible;
                cardList.remove(cardCounter);
                numberOfCards--; // the number of cards in the hand
            }

        }
        return target;
    }

    public Card removeRandomCard() {
        Card removedCard = null;
        while (removedCard == null && numberOfCards > 0) {
            int cardValue = (int)(Math.random() * 13);
            List<Card> cards = cardMap[cardValue];
            int size = cards.size();
            if (size > 0) {
                removedCard = cards.remove((int)(Math.random() * size));
                numberOfCards--;
            }
        }

        return removedCard;
    }

    public static Hand generateDeck() {
        List<Card> cards = new LinkedList();
        for (int suitCounter = 0; suitCounter < 4; suitCounter++) {
            for (int valueCounter = 0; valueCounter < 13; valueCounter++) {
                cards.add(new Card(valueCounter + 1, suitCounter));
            }
        }
        return new Hand(cards);
    }

    public static void main (String args[]) {
        Hand hand = Hand.generateDeck();
        System.out.println(hand.getNumberOfCards());
    }
}
