package solitaire.player;

import solitaire.cardutilities.Card;
import java.util.Scanner;
import solitaire.solitaireutilities.Move;

public class HumanPlayer extends Player {
    private Scanner scanner;

    public HumanPlayer () {
        scanner = new Scanner(System.in);
    }

    public Move chooseAction(Card[][] completeStacks,
                                     Card[][] solStacks,
                                      Card[] grabStack,
                             boolean grabStackReshuffle) {
        System.out.println("1: Move card from stack to suit stack");
        System.out.println("2: Move cards between stacks");
        System.out.println("3. Move card from grab pile");
        System.out.println("4. " + getGrabPileAction(grabStackReshuffle));
        System.out.print("Choose an action: ");
        int option = getUserInt();

        Move returnMove;
        switch (option) {
        case 1:
            returnMove = Move.ADD_TO_COMPLETE;
            break;
        case 2:
            returnMove = Move.MOVE_CARD_STACK;
            break;
        case 3:
            returnMove = Move.USE_GRAB;
            break;
        default:
            returnMove = Move.DRAW_CARD;
            break;
        }

        return returnMove;
    }

    public int[] moveCards (Card[][] solStacks) {
        int originatingStack;
        int numberOfCards;
        int targetStack;

        System.out.print("Pick the originating stack (1-7): ");
        originatingStack = getUserInt();

        System.out.print("Pick the target stack (1-7): ");
        targetStack = getUserInt();

        System.out.print("Pick the number of cards to move: ");
        numberOfCards = getUserInt();

        return new int[] {originatingStack, numberOfCards, targetStack};
    }

    public int moveToComplete(Card[][] completeStacks,
                              Card[][] solStacks) {
        System.out.println("Choose which stack you would like to move the top" +
                           " card from to its corresponding complete");
        System.out.println("Not all stacks can legally perform this move.");
        System.out.print("Stack(1-7): ");
        return getUserInt();
    }

    public int useGrabStack(Card grabCard,Card[][] solStacks) {
        System.out.println("Which stack would you like to move " +
                           grabCard.toString() + " to?");
        System.out.print("Stack(1-7) or Suit Complete (-1): ");
        return getUserInt();

    }

    private String getGrabPileAction(boolean reshuffle) {
        return reshuffle ? "Reshuffle dealing deck" :
            "Draw new card and add to grab pile";
    }

    private int getUserInt() {
        int value = 1;
        try {
            value = Integer.parseInt(scanner.nextLine());

        } catch (Exception ex) {
        }
        return value;
    }

}
