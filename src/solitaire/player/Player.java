package solitaire.player;

import solitaire.cardutilities.Card;
import java.util.Stack;
import solitaire.solitaireutilities.Move;

public abstract class Player {
    protected Stack<Integer> totalMoves;

    public Player () {
        totalMoves = new Stack();
    }

    public void addGame(int numberOfMoves) {
        totalMoves.push(numberOfMoves);
    }

    public Stack<Integer> getGameRecord() {
        return totalMoves;
    }

    public abstract Move chooseAction(Card[][] completeStacks,
                                     Card[][] solStacks,
                                      Card[] grabStack,
                                      boolean grabStackReshuffle);
    public abstract int[] moveCards (Card[][] solStacks);
    public abstract int moveToComplete(Card[][] completeStacks,
                                       Card[][] solStacks);
    public abstract int useGrabStack(Card grabCard,Card[][] solStacks);
}
