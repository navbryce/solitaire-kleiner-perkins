package solitaire.solitaireutilities;

import solitaire.cardutilities.Card;
import java.util.Stack;

public class CompleteStack {
    public final int suit;

    private Stack<Card> stack;

    public CompleteStack(int suit) {
        this.suit = suit;
        stack = new Stack();
    }


    /**
     * Assumes the move has already been checked as acceptible
     *
     * @return a <code>Card</code> value
     */
    public Card addCard (Card card) {
        return stack.push(card);
    }

    public boolean checkMove(Card card) {
        return card.suit == suit &&
            ((stack.size() == 0 && card.value == 1) ||
             (stack.size() !=0 && stack.peek().value + 1 == card.value));
    }

    public boolean stackComplete() {
        return stack.size() == 13;
    }

    public Card[] toArray() {
        return stack.toArray(new Card[stack.size()]);
    }

    @Override
    public String toString() {
        return stack.size() == 0 ? Card.getSuit(suit) : stack.peek().toString();
    }
}
