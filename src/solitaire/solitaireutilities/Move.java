package solitaire.solitaireutilities;

public enum Move {
    ADD_TO_COMPLETE,
    MOVE_CARD_STACK,
    DRAW_CARD,
    USE_GRAB;
}
