package solitaire.solitaireutilities;

import solitaire.cardutilities.Hand;
import solitaire.cardutilities.Card;
import java.util.List;
import java.util.LinkedList;
public class SolStack {

    private Hand hiddenCards;
    private int totalCards;
    private Hand visibleCards;

    public SolStack(List<Card> initialCards) {
        totalCards = initialCards.size();

        // initialize the visible and hidden cards
        visibleCards = new Hand();
        visibleCards.addCard(initialCards.remove(0));
        hiddenCards = new Hand(initialCards);
    }

    public Card[] addCardsToStack(Card[] cards) {
        Card[] cardsAdded;
        if (checkMove(cards)) {
            visibleCards.addCards(cards);

            // update the total cards value
            totalCards += cards.length;
            cardsAdded = cards;
        } else {
            // returns null if no cards were added
            cardsAdded = null;
        }
        if (totalCards != visibleCards.getNumberOfCards() + hiddenCards.getNumberOfCards()) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return cardsAdded;
    }

    public boolean checkMove(Card[] cards) {
        boolean valid;
        if (totalCards != 0) {

            Card topCard = cards[cards.length - 1]; // assumes greatest card at top
            Card bottomStack = visibleCards.getSortedArray()[0];

            valid = topCard.value == (bottomStack.value - 1)
                && (topCard.suit % 2 != bottomStack.suit % 2);
        } else {
            valid = true;
        }

        return valid;
    }


    /**
     * Describe <code>getKnownStack</code> method here.
     *
     * @return what the user would know about the stack. the top of the list
     * is the bottom of the stack
     */
    public Card[] getKnownStack() {
        Card[] cards = new Card[totalCards];
        Card[] visibleCards = getVisibleCards();

        int translation = totalCards - visibleCards.length;

        for (int cardCounter = 0; cardCounter < visibleCards.length;
             cardCounter++) {
            cards[visibleCards.length - 1 - cardCounter + translation] = visibleCards[cardCounter];
        }



        return cards;
    }

    public Card[] getVisibleCards() {
        return visibleCards.getSortedArray();
    }

    @Override
    public String toString() {
        String output = "Hidden Cards:";
        for (Card card: hiddenCards.getSortedArray()) {
            output += card.toString() + ", ";
        }
        output += "\nVisible Cards: ";

        for (Card card: visibleCards.getSortedArray()) {
            output += card.toString() + ", ";
        }
        output += "\n";
        return output;
    }

    public Card[] removeVisibleCards(int numberOfCards) {
        Card[] cards = visibleCards.getSortedArray();
        Card[] removedCards = new Card[numberOfCards];
        int cardCounter = 0;

        while (cardCounter < numberOfCards) {
            removedCards[cardCounter] = visibleCards.removeCard(cards[cardCounter]);


            if (removedCards[cardCounter] != null) {
                // an actual card was removed
                totalCards--;
            }
            cardCounter++;
        }

        // will check to see if a card needs to be revealed and will reveal if so
        revealCard();
        if (totalCards != visibleCards.getNumberOfCards() + hiddenCards.getNumberOfCards()) {
            try {
                throw new Exception();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return removedCards;

    }

    private Card revealCard() {
        Card revealedCard = null;
        if (visibleCards.getNumberOfCards() == 0
            && hiddenCards.getNumberOfCards() > 0) {
            revealedCard = hiddenCards.removeRandomCard();
            visibleCards.addCard(revealedCard);
        }
        return revealedCard;
    }

    public static void main(String[] args) {
        Card card2 = new Card(10, 0);
        LinkedList<Card> list = new LinkedList<Card>();
        list.add(card2);

        SolStack stack = new SolStack(list);
        System.out.println(stack);

        Card card3 = new Card(9, 1);
        stack.addCardsToStack(new Card[] {card3});

        System.out.println(stack);

        stack.removeVisibleCards(2);
        System.out.println(stack.getVisibleCards().length == 0);

        stack.addCardsToStack(new Card[]{new Card(9, 1), new Card(10, 2)});
        System.out.println(stack.getVisibleCards().length == 2);
        stack.addCardsToStack(new Card[]{new Card(7, 1), new Card(8, 2)});
        System.out.println(stack.getVisibleCards().length == 4);
        System.out.println(stack.getKnownStack().length == 4);


    }


}
