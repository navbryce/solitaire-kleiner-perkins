package solitaireutilities;

import cardutilities.Hand;
import cardutilities.Card;
import java.util.List;

public class Stack {
    
    private Hand hiddenCards;
    private int totalCards;
    private Hand visibleCards;
    
    public Stack(List<Card> initialCards) {
        totalCards = initialCards.size();

        // initialize the visible and hidden cards
        visibleCards = new Hand();
        visibleCards.addCard(initialCards.remove(0));
        hiddenCards = new Hand(initialCards);
    }

    public Card[] addCardsToStack(Card[] cards) {
        Card[] cardsAdded;
        if (checkMove(cards)) {
            visibleCards.addCards(cards);
            
            // update the total cards value
            totalCards += cards.length;
            cardsAdded = cards;
        } else {
            // returns null if no cards were added
            cardsAdded = null;
        }
        return cardsAdded;
    }

    public boolean checkMove(Card[] cards) {
        Card topCard = cards[cards.length - 1]; // assumes greatest card at top
        Card bottomStack = visibleCards.getSortedArray()[0];

        return topCard.value == (bottomStack.value - 1)
            && (topCard.suit % 2 != bottomStack.suit % 2);
    }

    
    /**
     * Describe <code>getKnownStack</code> method here.
     *
     * @return what the user would know about the stack. the top of the list
     * is the bottom of the stack
     */
    public Card[] getKnownStack() {
        Card[] cards = new Card[totalCards];
        Card[] visibleCards = getVisibleCards();

        int translation = totalCards - visibleCards.length;

        for (int cardCounter = visibleCards.length - 1; cardCounter >= 0;
             cardCounter--) {
            cards[cardCounter + translation] = visibleCards[cardCounter];
        }

        return cards;
    }

    public Card[] getVisibleCards() {
        return visibleCards.getSortedArray();
    }

    public Card[] removeVisibleCards(int numberOfCards) {
        Card[] cards = visibleCards.getSortedArray();
        Card[] removedCards = new Card[numberOfCards];
        int cardCounter = 0;
        while (cardCounter < numberOfCards) {
            removedCards[cardCounter] = visibleCards.removeCard(cards[cardCounter]);
            cardCounter++;
        }
        totalCards -= cardCounter; // the number of cards removed

        // will check to see if a card needs to be revealed and will reveal if so
        revealCard();
        
        return removedCards;
        
    }

    private Card revealCard() {
        Card revealedCard = null;
        if (visibleCards.getNumberOfCards() == 0
            && hiddenCards.getNumberOfCards() > 0) {
            revealedCard = hiddenCards.removeRandomCard();
            visibleCards.addCard(revealedCard);
        }
        return revealedCard;
    }

  
}
